package com.up.integration.runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

/**
 * 
 * @author Marcos Rachid
 *
 */
@RunWith(Cucumber.class)
@CucumberOptions(plugin = { "pretty",
		"html:target/cucumber" }, glue = "com.up.integration.steps", features = "classpath:features/campsite.feature")
public class RunCampsiteTest {}
